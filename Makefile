TARGET = argumentation


.PHONY: clean all


all : $(TARGET).pdf ;



clean :
	rm -f *.dvi *.aux *.log $(TARGET).pdf *.xbb


%.pdf : %.tex
	platex $*
	platex $*
	dvipdfmx $*
